//Begin file js15.js
      /* Description: 
        This program recieves an input from the user of a number in Fahrenheit, and converts it to Celcius and prints 
        it in the console.
      */

const js15 = function () {
    let fahrenheitInput = Number(prompt("Input a number in Fahrenheit to convert to Celcius"));
    let celcius = (5/9) * (fahrenheitInput - 32);
    console.log(fahrenheitInput + "°F is = " + celcius.toFixed(2) + "°C");

  }

  // window.addEventListener("load", js15);