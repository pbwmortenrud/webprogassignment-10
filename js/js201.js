/*
1: If the year is evenly divisible by 4, go to step 2. ...
2: If the year is evenly divisible by 100, go to step 3. ...
3: If the year is evenly divisible by 400, go to step 4. ...
4: The year is a leap year (it has 366 days).
5: The year is not a leap year (it has 365 days).
*/

/*
The program must accept input of a number from the user. Assume the number is a year. Make JavaScript write on the console 
true or false as to whether the year is a leap year or not.
 */

const js201 = function (e) {
  let year = prompt("Input a year #### to know if it's a leap year");

  //three conditions to find out the leap year
  if ((0 == year % 4 && 0 != year % 100) || 0 == year % 400) {
    console.log(true);
  } else {
    console.log(false);
  }

  // Do without if-statement
  // let isLeap =(year % 4 == 0 && !(year % 100 == 0)) || year % 100 ==0;

};
