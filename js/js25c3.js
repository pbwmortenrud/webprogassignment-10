const js25c3 = function () {
    let var1 = true;
    let var2 = true;
    let var3 = true;
    let result = (var1 == var2 == var3);

    console.log('Truth table for conjunction with 3 variables');
    console.log('var1\tvar2\tvar3\tvar1 && var2 && var 3');
    
    console.log(var1 + '\t' + var2 + '\t' + var3 + '\t' + result);

    var1 = true;
    var2 = true;
    var3 = false;
    result = var1 && var2 && var3;
    console.log(var1 + '\t' + var2 + '\t' + var3 + '\t' + result);

    var1 = true;
    var2 = false;
    var3 = true;
    result = var1 && var2 && var3;
    console.log(var1 + '\t' + var2 + '\t' + var3 + '\t' + result);

    var1 = false;
    var2 = true;
    var3 = true;
    result = var1 && var2 && var3;
    console.log(var1 + '\t' + var2 + '\t' + var3 + '\t' + result);

    var1 = true;
    var2 = false;
    var3 = false;
    result = var1 && var2 && var3;
    console.log(var1 + '\t' + var2 + '\t' + var3 + '\t' + result);

    var1 = false;
    var2 = true;
    var3 = false;
    result = var1 && var2 && var3;
    console.log(var1 + '\t' + var2 + '\t' + var3 + '\t' + result);

    var1 = false;
    var2 = false;
    var3 = true;
    result = var1 && var2 && var3;
    console.log(var1 + '\t' + var2 + '\t' + var3 + '\t' + result);

    var1 = false;
    var2 = false;
    var3 = false;
    result = var1 && var2 && var3;
    console.log(var1 + '\t' + var2 + '\t' + var3 + '\t' + result);

  };