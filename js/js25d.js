const js25d = function () {
    let var1 = true;
    let var2 = true;
    let result = var1 || var2;

    console.log('Truth table for disjunction');
    console.log('var1\tvar2\tvar1 || var2');
    
    console.log(var1 + '\t' + var2 + '\t' + result);

    var1 = true;
    var2 = false;
    result = var1 || var2;
    console.log(var1 + '\t' + var2 + '\t' + result);

    var1 = false;
    var2 = true;
    result = var1 || var2;
    console.log(var1 + '\t' + var2 + '\t' + result);

    var1 = false;
    var2 = false;
    result = var1 || var2;
    console.log(var1 + '\t' + var2 + '\t' + result);

  };